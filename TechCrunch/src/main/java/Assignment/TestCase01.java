package Assignment;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestCase01 {
	 WebDriver driver;
	    
		@BeforeTest
		public void AuthorForNews() throws InterruptedException {
			
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.get("https://techcrunch.com/");
			Thread.sleep(2000);
			
		}
		
		@Test(priority=1, description="Author Verification for News")
		public void Verification (){
			
			List<WebElement> myList = driver.findElements(By.xpath("//div[@class='river river--homepage ']//article//span[@class='river-byline__authors']"));
			int y=0;
			for(int i=0; i<myList.size(); i++) {
				y++;
				if(myList.get(i).isDisplayed()) {
					
					System.out.println(y + "." +  "article's author in the latest news is " + " " + myList.get(i).getText());
				}else {
					System.out.println("There is no author information for " + y + "article");
				}	
			}
			
		}
		@Test(priority=2, description="Image Verification for News")
		public void Verification2 () {
			
			List<WebElement> myimageList = driver.findElements(By.xpath("//div[@class='river river--homepage ']//article//footer//figure//img[@src]"));
			int y=0;
			System.out.println("\n");
			for(int i=0; i<myimageList.size(); i++) {
				y++;
				if(myimageList.get(i).isDisplayed()) {
					
					System.out.println(y + "." +  "article's image in the latest news exists");
				}else {
					System.out.println("There is no image for " + y + "article");
				
				}
				
			}
			
		}
		

}
